Soal 1 Membuat Database
CREATE DATABASE myshop; 

Soal 2 Membuat table di dalam Database
- Table Users
CREATE TABLE Users( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null ); 

- Table Categories
CREATE TABLE categories( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null );  

- Table Items
CREATE TABLE items( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, description varchar(255) NOT null, price int NOT null, stock int NOT null, category_id int NOT null, FOREIGN key(category_id) REFERENCES categories(id) ); 


Soal 3 Memasukkkan Data pada table
- Table users
INSERT INTO users(name, email, password) values("John Doe", "john@doe.com", "john123"),("Jane Doe", "jane@doe.com", "jenita123"); 

- Table categories
INSERT INTO categories(name) VALUES("gadget"),("cloth"),("men"),("women"),("branded"); 

- Table items
INSERT INTO items(name, description, price, stock, category_id) VALUES("Sumsang b50", "hape keren dari merek sumsang", "4000000", 100, 1),("Uniklooh", "baju keren dari brand ternama", "500000", 50, 2),("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", 10, 1); 


Soal 4 Mengambil Data dari Database
a. Mengambil data users, Buatlah sebuah query untuk mendapatkan data seluruh user pada table users. Sajikan semua field pada table users KECUALI password nya.
SELECT name, email from users; 

b. Mengambil Data
- Buatlah sebuah query untuk mendapatkan data item pada table yang memiliki harga di atas 1000000 (satu juta).
SELECT * from items WHERE price > 1000000; 

- Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci �uniklo�, �watch�, atau �sang� (pilih salah satu saja).
SELECT * from items where name like "%watch"; 

c. Menampilkan Data item join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS categories from items INNER JOIN categories on items.category_id = categories.id; 


Soal 5 Mengubah Data dari Database
UPDATE items set price=2500000 where id = 1; 